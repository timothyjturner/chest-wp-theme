<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      
    <?php wp_head(); ?>
      
    <link href="<?php echo get_template_directory_uri(); ?>/style.css" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri(); ?>/css/base.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/carousel.css" rel="stylesheet">
      
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>

    <title><?php wp_title(''); ?> - <?php bloginfo( 'name' ); ?></title>
      
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon">
  </head>
  <body class="chest-body">
      
    <header>
      
<?php
get_template_part('sections/utility-nav');
get_template_part('sections/main-nav');
        
        
?>  
        
  </header>
      
  <main role="main">