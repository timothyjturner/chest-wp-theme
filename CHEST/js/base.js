//Start Show and Hide Search Button

function showHideSearch() {
    showHideItem('chest-search-form-container');
}

//End Show and Hide Search Button




//Start Go to Top Button

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("chest-return-to-top").style.display = "block";
    } else {
        document.getElementById("chest-return-to-top").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}

//End Go to Top Button




//Start Functions General

function showHideItem(itemID) {
    var x = document.getElementById(itemID);
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}

//End Functions General