<?php

// Register Custom Navigation Walker
require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';

//Register Nav Menus
function register_my_menus() {
register_nav_menus( array(
	'primary_menu' => __( 'Primary Menu', 'CHEST' ),
    'utility_menu' => __( 'Utility Menu', 'CHEST' )
) );
}

add_action( 'init', 'register_my_menus' );
//End

//Displays Page Title on the home page, if one is not set
add_filter( 'wp_title', 'chest_home_page_title' );

function chest_home_page_title( $title )
{
  if ( empty( $title ) && ( is_home() || is_front_page() ) ) {
    $title = __( 'Home', 'textdomain' ) . ' | ' . get_bloginfo( 'description' );
  }
  return $title;
}
//End

//Add Theme Support for Post Thumbnails
add_theme_support( 'post-thumbnails', array( 'post', 'chest_sliders' ) );
add_theme_support( 'thumbnail', array( 'post', 'chest_sliders' ) );



/*******************************************************************
                    GENERAL HELPER FUNCTIONS
*******************************************************************/


//Change Default Excerpt Length
function custom_excerpt_length( $length ) {
	return 200;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

//Return custom excerpt link
function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).' [Read more...]';
  } else {
    $excerpt = implode(" ",$excerpt);
  }	
  $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
  return $excerpt;
}

function list_of_posts_categories() {
    $post_categories_objs = get_the_category();
    if($post_categories_objs != false) {
        foreach($post_categories_objs as $post_categories_obj) {
            $categories .= '<a href=' . get_category_link($post_categories_obj->cat_ID) . '>' . $post_categories_obj->name . '</a>, ';
        }
        $categories = substr($categories, 0, -2);
    }
    return $categories;
}

function list_of_posts_tags() {
    $post_tags_objs = get_the_tags();
    if($post_tags_objs != false) {
        foreach($post_tags_objs as $post_tags_obj) {
            $tags .= '<a href=' . get_term_link($post_tags_obj->term_id) . '>' . $post_tags_obj->name . '</a>, ';
        }
        $tags = substr($tags, 0, -2);
    }
    return $tags;
}

/*******************************************************************
                END GENERAL HELPER FUNCTIONS
*******************************************************************/



/*******************************************************************
                        THEME OPTIONS MENU
*******************************************************************/

//Admin Menu Hook
add_action( 'admin_menu', 'chest_theme_options_menu' );

//Add Admin Menu
function chest_theme_options_menu() {
	add_menu_page( 'CHEST Theme Options', 'Theme Options', 'manage_options', 'chest-theme-options', 'chest_theme_options', 'dashicons-admin-customizer', 61 );
	
	//call register settings function
	add_action( 'admin_init', 'register_chest_theme_settings' );
}

//Register Theme Settings & Set Initial Defaults
function register_chest_theme_settings() {
	register_setting( 'chest-theme-settings-group', 'chest_util_menu_icon' );
    if (get_option('chest_util_menu_icon') == '') { update_option('chest_util_menu_icon', get_template_directory_uri() . '/images/nav-icon-default.png'); } 

    register_setting( 'chest-theme-settings-group', 'chest_util_menu_style' );
    if (get_option('chest_util_menu_style') == '') { update_option('chest_util_menu_style', '1'); } 
    
    register_setting( 'chest-theme-settings-group', 'chest_cart_enabled' );
    if (get_option('chest_cart_enabled') == '') { update_option('chest_cart_enabled', '0'); } 
    
    register_setting( 'chest-theme-settings-group', 'chest_header_logo' );
    if (get_option('chest_header_logo') == '') { update_option('chest_header_logo', get_template_directory_uri() . '/images/logo-default.png'); } 
    
    register_setting( 'chest-theme-settings-group', 'chest_large_footer_enabled' );
    if (get_option('chest_large_footer_enabled') == '') { update_option('chest_large_footer_enabled', '0'); } 
    
    register_setting( 'chest-theme-settings-group', 'chest_large_footer_mobile_order' );
    if (get_option('chest_large_footer_mobile_order') == '') { update_option('chest_large_footer_mobile_order', '1,2,3,4'); } 
    
    register_setting( 'chest-theme-settings-group', 'chest_large_footer_back_color' );
    if (get_option('chest_large_footer_back_color') == '') { update_option('chest_large_footer_back_color', 'dcdcdc'); }    
    
    register_setting( 'chest-theme-settings-group', 'chest_google_map_api' );
    
    register_setting( 'chest-theme-settings-group', 'chest_small_footer_back_color' );
    if (get_option('chest_small_footer_back_color') == '') { update_option('chest_small_footer_back_color', '979797'); }    
}

//Allow Javascript Media API Usage in Admin Panel
add_action ( 'admin_enqueue_scripts', function () {
    if (is_admin ())
        wp_enqueue_media ();
} );

//Chest Theme Options Output
function chest_theme_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
    
?>    

<div class="wrap">
<h1>CHEST Theme Options</h1>    
 
<form method="post" action="options.php">
    <?php settings_fields( 'chest-theme-settings-group' ); ?>
    <?php do_settings_sections( 'chest-theme-settings-group' ); ?>
    <?php
	$wysiwyg_settings = array(
    'teeny' => true,
    'textarea_rows' => 2,
    'tabindex' => 1,
	'wpautop' => true,
	'media_buttons' => true
	);
    
    $default_options['util-menu-icon'] = get_template_directory_uri() . '/images/nav-icon-default.png';
    $default_options['util-menu-style'] = '1';
    $default_options['header-logo'] =  get_template_directory_uri() . '/images/logo-default.png';
	?>
    
    <br />
    
    <table class="form-table">
        <h2>Utility Menu</h2>
        <hr>
        <tr valign="top">       
        <th scope="row">Icon (disabled mobile | Recommended Size: 20w x 17h):</th>
        <td>
            <p>
                <img src="<?php echo (get_option('chest_util_menu_icon') !== '' ? get_option('chest_util_menu_icon') :  $default_options['util-menu-icon']); ?>" id="chest_util_menu_icon_image_disp"style="max-height:30px;margin:5px;" />
                <input type="text" value="<?php echo get_option('chest_util_menu_icon'); ?>" class="regular-text process_custom_images" id="util_menu_icon_container" style="display:none;" name="chest_util_menu_icon">
                <button class="set_util_menu_icon button">Choose Image</button>
            </p>
		</td>
        </tr>
        <tr valign="top">
        <th scope="row">Style of Utility Menu</th>
        <td><select name="chest_util_menu_style" id="util_menu_style" value="<?php echo get_option('chest_util_menu_style'); ?>">
		  <option value="1" <?php if(get_option('chest_util_menu_style') == 1)  echo 'selected="selected"'; ?> >1 - White</option>
		  <option value="2" <?php if(get_option('chest_util_menu_style') == 2)  echo 'selected="selected"'; ?> >2 - Dark</option>
		  <option value="3" <?php if(get_option('chest_util_menu_style') == 3)  echo 'selected="selected"'; ?> >3 - Light</option>		
		</select>
		</td>
        </tr>
    </table>
 
    <table class="form-table">
        <h2>eCommerce</h2>
        <hr>
        <tr valign="top">       
        <th scope="row">Shopping Cart</th>
        <td>
            <input type="checkBox" onclick="$(this).attr('value', this.checked ? 1 : 0)" value="<?php echo get_option('chest_cart_enabled'); ?>" <?php if(get_option('chest_cart_enabled') === '1') echo 'checked '; ?>id="chest-cart-enabled-cb" name="chest_cart_enabled"><label> Enabled</label>
		</td>
        </tr>
    </table>
    
    <table class="form-table">
        <h2>Main Header</h2>
        <hr>
        <tr valign="top">       
        <th scope="row">Main Logo (Recommended Size: 200w x 55h)</th>
        <td>
            <p>
                <img src="<?php echo (get_option('chest_header_logo') !== '' ? get_option('chest_header_logo') :  $default_options['header-logo']); ?>" id="chest_header_logo_image_disp"style="max-height:100px;margin:5px;" />
                <input type="text" value="<?php echo get_option('chest_header_logo'); ?>" class="regular-text process_custom_images" id="chest_header_logo_container" style="display:none;" name="chest_header_logo">
                <button class="set_header_image button">Choose Image</button>
            </p>
		</td>
        </tr>
    </table>
    
    <table class="form-table">        
        <h2>Footer Area</h2>
        <hr>
        <tr valign="top">       
        <th scope="row">Large Footer Section</th>
        <td>
            <input type="checkBox" onclick="$(this).attr('value', this.checked ? 1 : 0)" value="<?php echo get_option('chest_large_footer_enabled'); ?>" <?php if(get_option('chest_large_footer_enabled') === '1') echo 'checked '; ?>id="chest-large-footer-enabled-cb" name="chest_large_footer_enabled"><label> Enabled</label>
            <br /><br />
            <label>Order on Mobile: </label>
            <select name="chest_large_footer_mobile_order" id="util_menu_style" value="<?php echo get_option('chest_large_footer_mobile_order'); ?>">
              <?php $mobile_orders = [
                    '1,2,3,4','1,2,4,3','1,3,2,4','1,3,4,2','1,4,2,3','1,4,3,2',
                    '2,1,3,4','2,1,4,3','2,3,1,4','2,3,4,1','2,4,1,3','2,4,3,1',
                    '3,1,2,4','3,1,4,2','3,2,1,4','3,2,4,1','3,4,1,2','3,4,2,1',
                    '4,1,2,3','4,1,3,2','4,2,1,3','4,2,3,1','4,3,1,2','4,3,2,1'];
        
              foreach ($mobile_orders as $order) {
                  if (get_option('chest_large_footer_mobile_order') == $order) {
                      $option_checked = 'selected="selected"';
                  } else {
                      $option_checked = '';
                  }
                  echo '<option value="' . $order . '" ' . $option_checked . ' >' . $order . '</option>';
              }
              ?>
            </select>
            <br /><br />
            <label>Background Color: </label><input class="jscolor" value="<?php echo get_option('chest_large_footer_back_color'); ?>" name="chest_large_footer_back_color">
		</td>
        </tr>
        <tr valign="top">       
        <th scope="row">Google Maps API Key</th>
        <td>
            <p>
                <input class="widefat" type="textarea" id="chest-maps-api-setting-input" name="chest_google_map_api" value="<?php echo get_option('chest_google_map_api'); ?>">
            </p>
		</td>
        </tr>
        <tr valign="top">       
        <th scope="row">Small Footer Section</th>
        <td>
            <p>
                <label>Background Color: </label><input class="jscolor" value="<?php echo get_option('chest_small_footer_back_color'); ?>" name="chest_small_footer_back_color">
            </p>
		</td>
        </tr>
    </table>
    
    <?php submit_button(); ?>    

</form>
</div>

<!-- Script for Media Input on Admin Form -->
<script>
jQuery(document).ready(function() {
    var $ = jQuery;
    
    //Set Chest Util Menu Icon on Load
    if ($('.set_util_menu_icon').length > 0) {
        if ( typeof wp !== 'undefined' && wp.media && wp.media.editor) {
            $(document).on('click', '.set_util_menu_icon', function(e) {
                e.preventDefault();
                var button = $(this);
                var id = button.prev();
                wp.media.editor.send.attachment = function(props, attachment) {
                    id.val(attachment.url);
                    $('#util_menu_icon_container').attr('value',attachment.url);
                    $('#chest_util_menu_icon_image_disp').attr('src',attachment.url);
                };
                wp.media.editor.open(button);
                return false;
            });
        }
    }
    
    //Set Chest Header Logo on Load
    if ($('.set_header_image').length > 0) {
        if ( typeof wp !== 'undefined' && wp.media && wp.media.editor) {
            $(document).on('click', '.set_header_image', function(e) {
                e.preventDefault();
                var button = $(this);
                var id = button.prev();
                wp.media.editor.send.attachment = function(props, attachment) {
                    id.val(attachment.url);
                    $('#chest_header_logo_container').attr('value',attachment.url);
                    $('#chest_header_logo_image_disp').attr('src',attachment.url);
                };
                wp.media.editor.open(button);
                return false;
            });
        }
    }    
    
});
</script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jscolor.min.js"></script>
<?php }
//End Chest Theme Options Output

/*******************************************************************
                    END THEME OPTIONS MENU
*******************************************************************/



/*******************************************************************
                            WIDGETS
*******************************************************************/

//Register Side-bars
add_action( 'widgets_init', 'chest_widgets_init' );

function chest_widgets_init() {
    register_sidebar( array(
		'name'          => 'Right Sidebar',
		'id'            => 'right_sidebar',
	) );
    register_sidebar( array(
		'name'          => 'Blog Index: Right Sidebar',
		'id'            => 'right_sidebar_blog',
	) );
	register_sidebar( array(
		'name'          => 'Upper Footer: Left',
		'id'            => 'footer_upper_1',
	) );
    register_sidebar( array(
		'name'          => 'Upper Footer: Left Middle',
		'id'            => 'footer_upper_2',
	) );
    register_sidebar( array(
		'name'          => 'Upper Footer: Right Middle',
		'id'            => 'footer_upper_3',
	) );
    register_sidebar( array(
		'name'          => 'Upper Footer: Right',
		'id'            => 'footer_upper_4',
	) );
    
	register_sidebar( array(
		'name'          => 'Lower Footer: Left',
		'id'            => 'footer_lower_1',
	) );
    register_sidebar( array(
        'name'          => 'Lower Footer: Right',
		'id'            => 'footer_lower_2',
	) );
}

//Add About Chest Widget
include 'widgets/about-chest-widget.php';

//Add Chest Meeting Center Widget
include 'widgets/about-chest-meeting-widget.php';

//Add Contact Us Widget
include 'widgets/contact-us-widget.php';

//Add Follow Us Widget
include 'widgets/follow-us-widget.php';

//Add Follow Us Widget
include 'widgets/join-email-list-widget.php';

/*******************************************************************
                          END WIDGETS
*******************************************************************/


/*******************************************************************
                    SLIDER CUSTOM POST TYPE
*******************************************************************/

function chest_slider_post_type() {
    //Lables for Post type
    $labels = array(
      'singular_name'  => 'Slider Image',
      'add_new_item'  => 'Add New Slider',
      'edit_item'  => 'Edit Slider',
      'new_item'  => 'New Slider',
      'view_item'  => 'View Slider',
      'view_items'  => 'View Sliders',
      'search_items'  => 'Search Sliders',
      'not_found'  => 'No Sliders Found',
      'not_found_in_trash'  => 'No Sliders Found in Trash',
      'all_items'  => 'All Sliders',
      'archives'  => 'Slider Archives',
      'attributes'  => 'Slider Attributes',
      'insert_into_item'  => 'Insert Into Slider',
      'uploaded_to_this_item'  => 'Upload to this Slider',
      'featured_image'  => 'Slider Image',
      'set_featured_image'  => 'Set Slider Image',
      'remove_featured_image'  => 'Remove Slider Image',
      'use_featured_image'  => 'Use Slider Image',
    );
    
    //Post Type Settings
    $args = array(
      'public' => true,
      'supports' => array('title', 'thumbnail', 'revisions'),
      'label'  => 'Slider Images',
      'labels' => $labels,
      'menu_icon'  =>  'dashicons-images-alt2',
      'description' => 'Slider Images for the Front Page'
    );
    register_post_type( 'chest_sliders', $args );
}
add_action( 'init', 'chest_slider_post_type' );


//Add Custom Fields to Slider Post Type
add_action( 'admin_init', 'chest_slider_meta_items');

function chest_slider_meta_items() {
    add_meta_box('chest-slider-subititle-meta', 'Slider Content', 'chest_slider_meta_function', 'chest_sliders', 'normal', 'high');
}

//Slider Subtitle Options
function chest_slider_meta_function() {
    global $post;
    $custom = get_post_custom($post->ID);
    $subtitle = $custom['subtitle'][0];
    $button_enabled = $custom['button_enabled'][0];
    $button_text = $custom['button_text'][0];
    $button_link = $custom['button_link'][0];
    ?>
    <div class="chest_slider_content_field">
        <label class="chest_slider_content_field_label">Subtitle: </label>
        <input name="subtitle" value="<?php echo $subtitle; ?>" class="chest_slider_content_field_input" >
    </div>
    <br />
    <div class="chest_slider_content_field">
    <input type="checkBox" onclick="$(this).attr('value', this.checked ? 1 : 0)" value="<?php echo $button_enabled; ?>" <?php if($button_enabled === '1') echo 'checked '; ?>id="chest-slider-button-enabled" name="button_enabled"><label> Button Enabled</label>
    </div>
    <br />
    <div class="chest_slider_content_field">
        <label class="chest_slider_content_field_label">Button Text: </label>
        <input name="button_text" value="<?php echo $button_text; ?>" class="chest_slider_content_field_input">
    </div>
    <br />
    <div class="chest_slider_content_field">
        <label class="chest_slider_content_field_label">Button URL: </label>
        <input name="button_link" value="<?php echo $button_link; ?>" class="chest_slider_content_field_input">
    </div>
    <br />
    <div class="chest_slider_notes">
        <hr />
        <ul>
            <li><b>Tips:</b></li>
        <li>The Name of the Slider 'Post' will appear as the headline of the slider.</li>
        <li>Recommended Image Size is 1100 width by 622 height.</li>
        <li>The sliders are ordered to show the newest slider first. Simply change the 'Published On' date/time to alter the order.</li>
        </ul>
    </div>
    <style>
        .chest_slider_content_field_label, .chest_slider_content_field_input {
            display: block;
            width: 100%;
        }
    </style>
    <?php

}
add_action('save_post', 'save_slider_details');
function save_slider_details(){
  global $post;
 
  update_post_meta($post->ID, 'subtitle', $_POST['subtitle']);
  update_post_meta($post->ID, 'button_enabled', $_POST['button_enabled']);
  update_post_meta($post->ID, 'button_text', $_POST['button_text']);
  update_post_meta($post->ID, 'button_link', $_POST['button_link']);
}

/*******************************************************************
                  END SLIDER CUSTOM POST TYPE
*******************************************************************/



/*******************************************************************
                COURSE PAGE TEMPLATE CUSTOM OPTIONS
*******************************************************************/

add_action('add_meta_boxes', 'add_course_meta', 1);
function add_course_meta()
{
    global $post;

    if(!empty($post))
    {
        $pageTemplate = get_post_meta($post->ID, '_wp_page_template', true);

       if($pageTemplate == 'page-courses.php' )
        {
            add_meta_box(
                'course_meta', // $id
                'Course Information', // $title
                'display_course_information', // $callback
                'page', // $page
                'normal', // $context
                'high'); // $priority
        }
    }
}

function display_course_information()
{
    global $post;
    $custom = get_post_custom($post->ID);
    $dateLocation = $custom['dateLocation'][0];
    $chair = $custom['chair'][0];
    $coChair = $custom['coChair'][0];
    $pageStyle = $custom['pageStyle'][0];
    ?>
    <div class="chest_course_dateLocation_field">
        <label class="chest_course_dateLocation_field_label chest_course_detail_field_label">Date and Location: </label>
        <input name="dateLocation" value="<?php echo $dateLocation; ?>" class="chest_course_dateLocation_field_input chest_course_detail_field_input" >
    </div>
    <br />
    <div class="chest_course_chair_field">
        <label class="chest_course_chair_field_label chest_course_detail_field_label">Chair: </label>
        <input name="chair" value="<?php echo $chair; ?>" class="chest_course_chair_field_input chest_course_detail_field_input" >
    </div>
    <br />
    <div class="chest_course_coChair_field">
        <label class="chest_course_coChair_field_label chest_course_detail_field_label">Vice Chair: </label>
        <input name="coChair" value="<?php echo $coChair; ?>" class="chest_course_coChair_field_input chest_course_detail_field_input" >
    </div>
    <br />
    <hr>
    <div class="chest_course_pageStyle_field">
        <label class="chest_course_pageStyle_field_label">Page Style: </label>
        <select name="pageStyle" id="pageStyle" value="<?php echo $pageStyle; ?>">
		  <option value="dark" <?php if($pageStyle == 'dark')  echo 'selected="selected"'; ?> >Dark</option>
		  <option value="blue" <?php if($pageStyle == 'blue')  echo 'selected="selected"'; ?> >Blue</option>
		  <option value="orange" <?php if($pageStyle == 'orange')  echo 'selected="selected"'; ?> >Orange</option>		
		</select>
    </div>
    <style>
        .chest_course_detail_field_label, .chest_course_detail_field_input {
            display: block;
            width: 100%;
        }
    </style>
<?php
}

add_action('save_post', 'save_course_details');
function save_course_details(){
  global $post;
 
  update_post_meta($post->ID, 'dateLocation', $_POST['dateLocation']);
  update_post_meta($post->ID, 'chair', $_POST['chair']);
  update_post_meta($post->ID, 'coChair', $_POST['coChair']);
  update_post_meta($post->ID, 'pageStyle', $_POST['pageStyle']);
}

/*******************************************************************
              END COURSE PAGE TEMPLATE CUSTOM OPTIONS
*******************************************************************/