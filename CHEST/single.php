<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php
    $current_post_ID = get_the_ID();
?>

      <div class="chest-page-title-container container-fluid">
        <h1 class="chest-page-title"><?php echo get_the_title(); ?></h1>
      </div>
      <div class="chest-main-content-outer chest-single-post-content container-fluid" id="post-<?php the_ID(); ?>">
        <div class="chest-main-content-row row">
            <div class="chest-main-content col-lg-12">
                <div class="chest-post-time-date-author">
                    <p class="text-style-0"><?php the_date(); ?> by <?php the_author(); ?></p>
                </div>
                
                <?php the_content(); ?>
            </div>
            <?php endwhile; endif; ?>
        </div>
        
        <?php
        // WP_Query arguments
        $args = array(
            'post_type' => array( 'post' ),
        );

        // The Query
        $query = new WP_Query( $args );

        // The Loop
        if ( $query->have_posts() ) {
            echo '<div class="chest-recent-posts-row row"><h1 id="chest-recent-posts-title">Recent Posts</h1><br /><div class="chest-post-preview-outer-container">';
            $cnt = 0;
            while ( (($query->have_posts()) and ($cnt < 5) ) ) {
                $query->the_post();
                $preview_post_ID = get_the_ID();
                if($preview_post_ID != $current_post_ID) {
                    echo '<a class="chest-post-preview-link" href="' . get_post_permalink()  . '"><div class="chest-post-preview-container">
                    <div class="chest-post-thumb-container"><img src="' . get_the_post_thumbnail_url()  . '"></div><br />
                    <p>' . get_the_title() . '</p>
                    </div></a>';
                    $cnt++;
                }
            }
            echo '</div></div>';
        }

        // Restore original Post Data
        wp_reset_postdata();  
        ?>
        <div class="chest-post-info-row row"> 
            <p class="text-style-0">Category: <?php echo list_of_posts_categories(); ?></p>
            <p class="text-style-0">Related Topics: <?php echo list_of_posts_tags(); ?></p>
        </div>    
    </div>  


<?php get_footer(); ?>