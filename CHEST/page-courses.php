<?php
/*
Template Name: Course Page
*/
?>
<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php
    $pageStyle = get_post_meta(get_the_ID(), 'pageStyle');
    $chair = get_post_meta(get_the_ID(), 'chair');
    $coChair = get_post_meta(get_the_ID(), 'coChair');
    $dateLocation = get_post_meta(get_the_ID(), 'dateLocation');
?>
      <section class="chest-course-page-style-<?php echo $pageStyle[0]; ?>" id="chest-course-page-title-section">
          <div class="chest-page-title-container container-fluid" id="chest-course-page-title">
            <h1 class="chest-page-title"><?php echo get_the_title(); ?></h1>
          </div>
          <?php if($dateLocation[0] != '') { ?>
          <div class="chest-page-title-container container-fluid" id="chest-course-page-dateLocation">
            <h3 class="chest-page-title"><?php echo $dateLocation[0]; ?></h3>
          </div>
          <?php } ?>
          <?php if( (($chair[0] != '') or ($coChair[0] != '')) ) { ?>
          <div class="chest-page-title-container container-fluid" id="chest-course-page-chairs">
            <?php if($chair[0] != '') { ?>
            <h3 class="chest-page-title">Chair: <?php echo $chair[0]; ?></h3>
             <?php } ?>
            <?php if($coChair[0] != '') { ?> 
            <h3 class="chest-page-title">Co-Chair: <?php echo $coChair[0]; ?></h3>
            <?php } ?>
          </div>
          <?php } ?>
      </section>
      <div class="chest-main-content-outer container-fluid" id="page-<?php the_ID(); ?>">
        <div class="chest-main-content-row row">
            <div class="chest-main-content col-lg-12">
                <?php the_content(); ?>
            </div>
            <?php endwhile; endif; ?>
        </div>
      </div>

<?php get_footer(); ?>