<?php get_header(); ?>
<?php
// WP_Query arguments
$paged = ( get_query_var( 'page' ) ) ? absint( get_query_var( 'page' ) ) : 1;
$args = array(
    's' => $_GET["s"],
    'paged' => $paged,
    'post_type' => array( 'post', 'page' ),                 
	'posts_per_page'         => '10',
);


// The Query
$query = new WP_Query( $args );
?>
      <div class="chest-page-title-container container-fluid">
        <h1 class="chest-page-title">Search Results For: <?php echo $_GET["s"] ?></h1>
      </div>
      <div class="chest-main-content-outer chest-search-page container-fluid" id="page-search">
        <div class="chest-main-content-row row">
        <div class="chest-main-content col-lg-12">
<?php
if ( $query->have_posts() ) {
	while ( $query->have_posts() ) {
        $query->the_post();
        ?>
            <a class="chest-search-item-link" href="<?php echo get_the_permalink(); ?>">
                <div class="chest-search-item">
                    <h1 class="chest-search-post-title"><?php the_title(); ?></h1>
                    <div class="chest-search-item-main">
                            <p class="chest-search-excerpt">
                                <?php echo excerpt(100); ?>
                            </p>
                    </div>
                </div>
            </a>
            <br />
        <?php 
	}
    echo paginate_links( array(
        'base' => get_home_url() . '/?s=' . $_GET["s"] . '&%_%',
        'format' => 'page=%#%',
        'current' => max( 1, get_query_var('page') ),
        'total' => $query->max_num_pages
    ) );
} else {
	echo '<p>Sorry, your search returned no results.</p><p><a href="' . get_home_url() .'">Click Here to Go to Homepage</a></p>';
}
            ?>
            
            </div></div></div>

<?php get_footer(); ?>