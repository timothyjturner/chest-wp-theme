
<?php if(get_option('chest_large_footer_enabled') == '1') {
        get_template_part('sections/footer-upper');
      }

        get_template_part('sections/footer-lower'); ?>

      <button onclick="topFunction()" id="chest-return-to-top" title="Go to top"><span class="fa-stack fa-lg"><i class="far fa-circle fa-stack-2x"></i><i class="fas fa-angle-up fa-stack-1x fa-2x chest-social-icon"></i></span></button>
    </main>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <!-- Theme JavaScript -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/base.js"></script>

<?php wp_footer(); ?>
    <!-- Footer End -->
  </body>
</html>