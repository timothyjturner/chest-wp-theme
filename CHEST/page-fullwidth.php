<?php
/*
Template Name: Full Width Page
*/
?>
<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <div class="chest-page-title-container container-fluid">
        <h1 class="chest-page-title"><?php echo get_the_title(); ?></h1>
      </div>
      <div class="chest-main-content-outer container-fluid" id="page-<?php the_ID(); ?>">
        <div class="chest-main-content-row row">
            <div class="chest-main-content col-lg-12">
                <?php the_content(); ?>
            </div>
            <?php endwhile; endif; ?>
        </div>
      </div>

<?php get_footer(); ?>