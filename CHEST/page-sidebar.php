<?php
/*
Template Name: Page with Sidebar
*/
?>
<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <div class="chest-page-title-container container-fluid">
        <h1 class="chest-page-title"><?php echo get_the_title(); ?></h1>
      </div>
      <div class="chest-main-content-outer container-fluid" id="page-<?php the_ID(); ?>">
        <div class="chest-main-content-row row">
            <div class="chest-main-content chest-main-content-w-sidebar col-lg-9">
                <?php the_content(); ?>
            </div>
            <?php endwhile; endif; ?>
                <?php
                if(is_active_sidebar( 'right_sidebar' )) {
                    echo '<div class="chest-right-sidebar-content col-lg-3">';
                        dynamic_sidebar( 'right_sidebar' );
                    echo '</div>';
                }
                ?>
        </div>
      </div>

<?php get_footer(); ?>