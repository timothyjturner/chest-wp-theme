<?php get_header(); ?>

<?php get_template_part('sections/front-carousel'); ?>
            
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <div class="chest-main-content-outer container-fluid" id="page-<?php the_ID(); ?>">
        <div class="chest-main-content-row row">
            <div class="chest-main-content col-lg-12">
                <?php the_content(); ?>
            </div>
            <?php endwhile; endif; ?>
        </div>
      </div>

<?php get_footer(); ?>