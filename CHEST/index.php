<?php get_header(); ?>

      <div class="chest-main-content-outer container">
        <div class="chest-main-content-row row">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <div class="chest-main-content col">
                    <a href="<?php echo get_the_permalink(); ?>">
                        <article class="post">
                            <div class="post-container">
                                <div class="post-thumb">
                                    <div class="post-preview">
                                        <h3 class="post-grid-title"><?php the_title(); ?></h3>
                                        <p><?php echo substr(get_the_excerpt(), 0, 200); ?></p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </a>
                </div>   
            <?php endwhile; endif; ?>
        </div>
      </div>

<?php get_footer(); ?>