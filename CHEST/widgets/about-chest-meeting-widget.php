<?php
//About Chest Meeting Center Widget

add_action( 'widgets_init', function() { register_widget( 'About_CHEST_Meeting_Widget' ); } );
class About_CHEST_Meeting_Widget extends WP_Widget {
 
    public function __construct() {
        // actual widget processes
        parent::__construct(
            'chest_about_meeting_widget', // Base ID
            'CHEST About Meeting Center', // Name
            array( 'description' => __( 'Displays About Meeting Center Section, for use in upper footer.', 'text_domain' ), 
                   
                 ) // Args
        );
    }
 
    public function widget( $args, $instance ) {
        // outputs the content of the widget
        extract( $args );
        $title = apply_filters( 'widget_title', $instance['title'] );
 
        echo '<h1 class="chest-upper-footer-heading">' . $instance['title'] . '</h1><p class="text-style-0">' . $instance['description'] . '</p>';
    }
 
    public function form( $instance ) {
        // outputs the options form in the admin
    $title = $instance[ 'title' ];
    $description = $instance[ 'description' ];
     
    // markup for form ?>
    <p>
        <label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
        <input class="widefat" type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $title ); ?>">
        <br />
        <label for="<?php echo $this->get_field_id( 'description' ); ?>">Description:</label>
        <input class="widefat" type="textarea" id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>" value="<?php echo esc_attr( $description ); ?>">
    </p>
             
<?php
    }
 
    public function update( $new_instance, $old_instance ) {
        // processes widget options to be saved
        $instance = array();
        $instance['title'] = ( !empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : 'About the CHEST Meeting Center';
        
        $instance['description'] = ( !empty( $new_instance['description'] ) ) ? strip_tags( $new_instance['description'] ) : 'The Innovation, Simulation, and Training Center is a state-of-the-art facility fully equipped to host meetings and events, medical meetings, simulation training, device testing, or ethnographic research.<br /><br />Located in Glenview, Illinois, we serve the Chicagoland area, including Northbrook, Deerfield, Northfield, Lincolnshire, Evanston, and the northern Chicago Suburbs.';
        
        return $instance;
    }
 
}
//End About Chest Widget