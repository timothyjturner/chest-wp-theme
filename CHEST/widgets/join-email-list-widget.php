<?php
//Join Email List

add_action( 'widgets_init', function() { register_widget( 'Join_Email_List_CHEST_Widget' ); } );
class Join_Email_List_CHEST_Widget extends WP_Widget {
 
    public function __construct() {
        // actual widget processes
        parent::__construct(
            'join_email_list_widget', // Base ID
            'CHEST Join Email List', // Name
            array( 'description' => __( 'Displays A Form to Join Email List, for use in upper footer.', 'text_domain' ), 
                   
                 ) // Args
        );
    }
 
    public function widget( $args, $instance ) {
        // outputs the content of the widget
        extract( $args );
        $title = apply_filters( 'widget_title', $instance['title'] );
        
        echo '<h1 class="chest-upper-footer-heading">JOIN OUR EMAIL LIST</h1>
                <form class="chest-footer-upper-subscribe-form">
                    <input type="text" name="email-sub" placeholder="Email address">
                    <br /><br />
                    <button type="submit">Subscribe</button>
                </form>';
        
    }
 
    public function form( $instance ) {
        // outputs the options form in the admin
     
    // markup for form ?>
    <p>

    </p>
             
<?php
    }
 
    public function update( $new_instance, $old_instance ) {
        // processes widget options to be saved
        $instance = array();
        
        return $instance;
    }
 
}
//End About Chest Widget