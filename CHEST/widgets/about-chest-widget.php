<?php
//About Chest Widget

add_action( 'widgets_init', function() { register_widget( 'About_CHEST_Widget' ); } );
class About_CHEST_Widget extends WP_Widget {
 
    public function __construct() {
        // actual widget processes
        parent::__construct(
            'chest_about_widget', // Base ID
            'CHEST About', // Name
            array( 'description' => __( 'Displays About Section, for use in upper footer.', 'text_domain' ), 
                   
                 ) // Args
        );
    }
 
    public function widget( $args, $instance ) {
        // outputs the content of the widget
        extract( $args );
        $title = apply_filters( 'widget_title', $instance['title'] );
 
        echo '<h1 class="chest-upper-footer-heading">' . $instance['title'] . '</h1><p class="text-style-0">' . $instance['description'] . '</p>
                <a href="' . esc_url($instance['link_url']) . '" class="chest-footer-learn-link"><h3>' . $instance['link_text'] . '</h3></a>';
    }
 
    public function form( $instance ) {
        // outputs the options form in the admin
    $title = $instance[ 'title' ];
    $description = $instance[ 'description' ];
    $link_text = $instance[ 'link_text' ];
    $link_url = $instance[ 'link_url' ];
     
    // markup for form ?>
    <p>
        <label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
        <input class="widefat" type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $title ); ?>">
        <br />
        <label for="<?php echo $this->get_field_id( 'description' ); ?>">Description:</label>
        <input class="widefat" type="textarea" id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>" value="<?php echo esc_attr( $description ); ?>">
        <br />
        <label for="<?php echo $this->get_field_id( 'link_text' ); ?>">Link Text:</label>
        <input class="widefat" type="text" id="<?php echo $this->get_field_id( 'link_text' ); ?>" name="<?php echo $this->get_field_name( 'link_text' ); ?>" value="<?php echo esc_attr( $link_text ); ?>">
        <br />
        <label for="<?php echo $this->get_field_id( 'link_url' ); ?>">Link URL:</label>
        <input class="widefat" type="text" id="<?php echo $this->get_field_id( 'link_url' ); ?>" name="<?php echo $this->get_field_name( 'link_url' ); ?>" value="<?php echo esc_attr( $link_url ); ?>">
        <script>
        
        </script>
    </p>
             
<?php
    }
 
    public function update( $new_instance, $old_instance ) {
        // processes widget options to be saved
        $instance = array();
        $instance['title'] = ( !empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : 'ABOUT THE CHEST FOUNDATION';
        
        $instance['description'] = ( !empty( $new_instance['description'] ) ) ? strip_tags( $new_instance['description'] ) : 'The mission of the CHEST Foundation is to champion lung health by supporting clinical resarch, community service, and patient education.';
        
        $instance['link_text'] = ( !empty( $new_instance['link_text'] ) ) ? strip_tags( $new_instance['link_text'] ) : 'LEARN MORE!';
        
        $instance['link_url'] = ( !empty( $new_instance['link_url'] ) ) ? strip_tags( $new_instance['link_url'] ) : '#';
        
        return $instance;
    }
 
}
//End About Chest Widget