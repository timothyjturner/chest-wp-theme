<?php
//Follow Us Widget

add_action( 'widgets_init', function() { register_widget( 'Follow_Us_CHEST_Widget' ); } );
class Follow_Us_CHEST_Widget extends WP_Widget {
 
    public function __construct() {
        // actual widget processes
        parent::__construct(
            'chest_follow_us_widget', // Base ID
            'CHEST Follow Us', // Name
            array( 'description' => __( 'Displays Social Media Links, for use in upper footer.', 'text_domain' ), 
                   
                 ) // Args
        );
    }
 
    public function widget( $args, $instance ) {
        // outputs the content of the widget
        extract( $args );
        $title = apply_filters( 'widget_title', $instance['title'] );
        
        echo '<h1 class="chest-upper-footer-heading">FOLLOW US</h1>';
        
        $page_link = $instance['facebook_link'];
        $icon_class = 'fab fa-facebook-f fa-stack-1x chest-social-icon';
        if ($page_link != '') {
            echo '<a href="' . $page_link . '" target="_blank" class="chest-social-icon-container"><span class="fa-stack fa-lg"><i class="fas fa-circle fa-stack-2x chest-social-circle"></i><i class="' . $icon_class . '"></i></span></a>';
        }
        
        $page_link = $instance['linkedIn_link'];
        $icon_class = 'fab fa-linkedin-in fa-stack-1x chest-social-icon';
        if ($page_link != '') {
            echo '<a href="' . $page_link . '" target="_blank" class="chest-social-icon-container"><span class="fa-stack fa-lg"><i class="fas fa-circle fa-stack-2x chest-social-circle"></i><i class="' . $icon_class . '"></i></span></a>';
        }
        
        $page_link = $instance['pinterest_link'];
        $icon_class = 'fab fa-pinterest-p fa-stack-1x chest-social-icon';
        if ($page_link != '') {
            echo '<a href="' . $page_link . '" target="_blank" class="chest-social-icon-container"><span class="fa-stack fa-lg"><i class="fas fa-circle fa-stack-2x chest-social-circle"></i><i class="' . $icon_class . '"></i></span></a>';
        }
        
        $page_link = $instance['twitter_link'];
        $icon_class = 'fab fa-twitter fa-stack-1x chest-social-icon';
        if ($page_link != '') {
            echo '<a href="' . $page_link . '" target="_blank" class="chest-social-icon-container"><span class="fa-stack fa-lg"><i class="fas fa-circle fa-stack-2x chest-social-circle"></i><i class="' . $icon_class . '"></i></span></a>';
        }
        

    }
 
    public function form( $instance ) {
        // outputs the options form in the admin
    $email = $instance[ 'facebook_link' ];
    $email = $instance[ 'linkedIn_link' ];
    $email = $instance[ 'pinterest_link' ];
    $email = $instance[ 'twitter_link' ];
     
    // markup for form ?>
    <p>
        <label for="<?php echo $this->get_field_id( 'facebook_link' ); ?>">Facebook Page Link:</label>
        <input class="widefat" type="text" id="<?php echo $this->get_field_id( 'facebook_link' ); ?>" name="<?php echo $this->get_field_name( 'facebook_link' ); ?>" value="<?php echo esc_attr( $instance['facebook_link'] ); ?>">
        <br />
        <label for="<?php echo $this->get_field_id( 'linkedIn_link' ); ?>">LinkedIn Page Link:</label>
        <input class="widefat" type="text" id="<?php echo $this->get_field_id( 'linkedIn_link' ); ?>" name="<?php echo $this->get_field_name( 'linkedIn_link' ); ?>" value="<?php echo esc_attr( $instance['linkedIn_link'] ); ?>">
        <br />
        <label for="<?php echo $this->get_field_id( 'pinterest_link' ); ?>">Pinterest Page Link:</label>
        <input class="widefat" type="text" id="<?php echo $this->get_field_id( 'pinterest_link' ); ?>" name="<?php echo $this->get_field_name( 'pinterest_link' ); ?>" value="<?php echo esc_attr( $instance['pinterest_link'] ); ?>">
        <br />
        <label for="<?php echo $this->get_field_id( 'twitter_link' ); ?>">Twitter Page Link:</label>
        <input class="widefat" type="text" id="<?php echo $this->get_field_id( 'twitter_link' ); ?>" name="<?php echo $this->get_field_name( 'twitter_link' ); ?>" value="<?php echo esc_attr( $instance['twitter_link'] ); ?>">
    </p>
             
<?php
    }
 
    public function update( $new_instance, $old_instance ) {
        // processes widget options to be saved
        $instance = array();
        
        $instance['facebook_link'] = ( !empty( $new_instance['facebook_link'] ) ) ? strip_tags( $new_instance['facebook_link'] ) : '';
        
        $instance['linkedIn_link'] = ( !empty( $new_instance['linkedIn_link'] ) ) ? strip_tags( $new_instance['linkedIn_link'] ) : '';
        
        $instance['pinterest_link'] = ( !empty( $new_instance['pinterest_link'] ) ) ? strip_tags( $new_instance['pinterest_link'] ) : '';
        
        $instance['twitter_link'] = ( !empty( $new_instance['twitter_link'] ) ) ? strip_tags( $new_instance['twitter_link'] ) : '';
    
        
        return $instance;
    }
 
}
//End About Chest Widget