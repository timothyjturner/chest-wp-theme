<?php
//Contact Us Widget

add_action( 'widgets_init', function() { register_widget( 'Contact_Us_CHEST_Widget' ); } );
class Contact_Us_CHEST_Widget extends WP_Widget {
 
    public function __construct() {
        // actual widget processes
        parent::__construct(
            'chest_contact_us_widget', // Base ID
            'CHEST Contact Us', // Name
            array( 'description' => __( 'Displays Contact Info, for use in upper footer.', 'text_domain' ), 
                   
                 ) // Args
        );
    }
 
    public function widget( $args, $instance ) {
        // outputs the content of the widget
        extract( $args );
        $title = apply_filters( 'widget_title', $instance['title'] );
        
        echo '<h1 class="chest-upper-footer-heading">CONTACT US</h1>';
        
        $email = $instance['email'];
        if ($email != '') {
            echo '<span class="chest-contact-container"><i class="fas fa-envelope"></i><div class="chest-contact-info"><a href="mailto:' . $email . '" class="text-style-0">' .  $email . '</a></div></span>
                <br />';
        }
        
        $phone1 = $instance['phone1'];
        $phone2 = $instance['phone2'];
        if (($phone1 != '') && ($phone2 !='')) {
            echo '<span class="chest-contact-container"><i class="fas fa-phone" data-fa-transform="flip-h"></i><div class="chest-contact-info"><a href="tel:' . $phone1 .'" class="text-style-0"> ' . $phone1 . '</a><br /> <a href="tel:' . $phone2 .'" class="text-style-0"> ' . $phone2 . '</a></div></span>
                <br />';
        } elseif ($phone1 != '') {
             echo '<span class="chest-contact-container"><i class="fas fa-phone" data-fa-transform="flip-h"></i><div class="chest-contact-info"><a href="tel:' . $phone1 .'" class="text-style-0"> ' . $phone1 . '</a></div></span>
                <br />';           
        } elseif ($phone2 != '') {
            echo '<span class="chest-contact-container"><i class="fas fa-phone" data-fa-transform="flip-h"></i><div class="chest-contact-info"><br /> <a href="tel:' . $phone2 .'" class="text-style-0"> ' . $phone2 . '</a></div></span>
                <br />';            
        }
            
        $address1 = $instance['address1'];
        $address2 = $instance['address2'];
        $address = $address1 . ' ' . $address2;
        $search_phrase = str_replace(' ', '+', $address); 
        $search_phrase = str_replace(',', '+', $search_phrase); 
        if ($address1 != '' || $address2 != '' ) {
            echo '<span class="chest-contact-container"><i class="fas fa-home" data-fa-transform="flip-h"></i><div class="chest-contact-info"><a href="https://www.google.com/maps/place/' . $search_phrase . '" target="_blank" class="text-style-0">' . $address1 . '<br />' . $address2 . '</a></div></span>
                <br />';
        }
            
        $enable_chat = $instance['enable_chat'];
        if ($enable_chat == '1') {
            echo '<span class="chest-contact-container"><i class="fas fa-comments"></i><div class="chest-contact-info"><a href="#" class="chest-footer-need-help-link"> <h3> NEED HELP NOW?<br /> CLICK TO CHAT</h3></a></div></span>';
        }
            
        $enable_map = $instance['enable_map'];
        $map_api_key = get_option('chest_google_map_api');
        if ($enable_map == '1') {
            echo '<div class="chest-contact-map"><iframe
                  width="450" height=450
                  frameborder="0" style="border:0"
                  src="https://www.google.com/maps/embed/v1/place?key=' . $map_api_key . '&q=' . $search_phrase . '">
                </iframe></div>';
        }
    }
 
    public function form( $instance ) {
        // outputs the options form in the admin
    $email = $instance[ 'email' ];
    $phone1 = $instance[ 'phone1' ];
    $phone2 = $instance[ 'phone2' ];
    $address1 = $instance[ 'address1' ];
    $address2 = $instance[ 'address2' ];
    $enable_chat = $instance[ 'enable_chat' ];
    $enable_map = $instance[ 'enable_map' ];
     
    // markup for form ?>
    <p>
        <label for="<?php echo $this->get_field_id( 'email' ); ?>">Email:</label>
        <input class="widefat" type="text" id="<?php echo $this->get_field_id( 'email' ); ?>" name="<?php echo $this->get_field_name( 'email' ); ?>" value="<?php echo esc_attr( $email ); ?>">
        <br />
        <label for="<?php echo $this->get_field_id( 'phone1' ); ?>">Phone 1:</label>
        <input class="widefat" type="textarea" id="<?php echo $this->get_field_id( 'phone1' ); ?>" name="<?php echo $this->get_field_name( 'phone1' ); ?>" value="<?php echo esc_attr( $phone1 ); ?>">
        <br />
        <label for="<?php echo $this->get_field_id( 'phone2' ); ?>">Phone 2:</label>
        <input class="widefat" type="textarea" id="<?php echo $this->get_field_id( 'phone2' ); ?>" name="<?php echo $this->get_field_name( 'phone2' ); ?>" value="<?php echo esc_attr( $phone2 ); ?>">
        <br />
        <label for="<?php echo $this->get_field_id( 'address1' ); ?>">Address Line 1:</label>
        <input class="widefat" type="textarea" id="<?php echo $this->get_field_id( 'address1' ); ?>" name="<?php echo $this->get_field_name( 'address1' ); ?>" value="<?php echo esc_attr( $address1 ); ?>">
        <br />   
        <label for="<?php echo $this->get_field_id( 'address2' ); ?>">Address Line 2:</label>
        <input class="widefat" type="textarea" id="<?php echo $this->get_field_id( 'address2' ); ?>" name="<?php echo $this->get_field_name( 'address2' ); ?>" value="<?php echo esc_attr( $address2 ); ?>">
        <br />  
        <input type="checkBox" onclick="$(this).attr('value', this.checked ? 1 : 0)" value="<?php echo $enable_chat; ?>" <?php if($enable_chat == '1') echo ' checked '; ?>id="<?php echo $this->get_field_id( 'enable_chat' ); ?>" name="<?php echo $this->get_field_name( 'enable_chat' ); ?>"><label> Chat Enabled</label>
        <br />
        <input type="checkBox" onclick="$(this).attr('value', this.checked ? 1 : 0)" value="<?php echo $enable_map; ?>" <?php if($enable_map == '1') echo ' checked '; ?>id="<?php echo $this->get_field_id( 'enable_map' ); ?>" name="<?php echo $this->get_field_name( 'enable_map' ); ?>"><label> Map Enabled (needs api key, see theme options)</label>
        
    </p>
             
<?php
    }
 
    public function update( $new_instance, $old_instance ) {
        // processes widget options to be saved
        $instance = array();
        
        $instance['email'] = ( !empty( $new_instance['email'] ) ) ? strip_tags( $new_instance['email'] ) : '';
        
        $instance['phone1'] = ( !empty( $new_instance['phone1'] ) ) ? strip_tags( $new_instance['phone1'] ) : '';
  
        $instance['phone2'] = ( !empty( $new_instance['phone2'] ) ) ? strip_tags( $new_instance['phone2'] ) : '';
        
        $instance['address1'] = ( !empty( $new_instance['address1'] ) ) ? strip_tags( $new_instance['address1'] ) : '';
        
        $instance['address2'] = ( !empty( $new_instance['address2'] ) ) ? strip_tags( $new_instance['address2'] ) : '';
        
        $instance['enable_chat'] = ( !empty( $new_instance['enable_chat'] ) ) ? strip_tags( $new_instance['enable_chat'] ) : '0';
        
        $instance['enable_map'] = ( !empty( $new_instance['enable_map'] ) ) ? strip_tags( $new_instance['enable_map'] ) : '0';
        
        return $instance;
    }
 
}
//End About Chest Widget