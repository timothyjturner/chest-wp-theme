<?php get_header(); ?>
<?php 
    $blog_page_url =  get_permalink( get_option( 'page_for_posts' ) );
    $blog_page_url = substr($blog_page_url, 0, -1);
?>
      <div class="chest-page-title-container container-fluid">
        <h1 class="chest-page-title">BLOG</h1>
      </div>
      <div class="chest-main-content-outer chest-blog-index-page container-fluid" id="page-<?php the_ID(); ?>">
        <div class="chest-main-content-row row">
        <div class="chest-main-content chest-main-content-w-sidebar col-lg-9">
<?php 
// WP_Query arguments
$paged = ( get_query_var( 'page' ) ) ? absint( get_query_var( 'page' ) ) : 1;
$args = array(
	'post_type'              => array( 'post' ),
	'paged'               =>  $paged,
	'posts_per_page'         => '6',
);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) {
	while ( $query->have_posts() ) {
        $query->the_post();
        ?>
            <a class="chest-blog-post-item-link" href="<?php echo get_the_permalink(); ?>">
                <div class="chest-blog-post-item">
                    <h1 class="chest-blog-post-title"><?php the_title(); ?></h1>
                    <div class="chest-post-time-date-author">
                        <p class="text-style-0"><?php echo get_the_date(); ?> by <?php the_author(); ?></p>
                    </div>
                    <div class="chest-blog-post-item-main row">
                        <div class="chest-blog-post-item-left col-lg-4">
                            <img class="chest-blog-post-thumbnail" src="<?php echo get_the_post_thumbnail_url(); ?>">
                        </div>
                        <div class="chest-blog-post-item-right col-lg-8">
                            <p class="chest-blog-post-excerpt">
                                <?php echo excerpt(49); ?>
                            </p>
                        </div>
                    </div>
                </div>
            </a>
            <div class="chest-blog-post-item-bottom">
                <p class="text-style-0">Category: <?php echo list_of_posts_categories(); ?></p>
                <p class="text-style-0">Related Topics: <?php echo list_of_posts_tags(); ?></p>
            </div>
            <br />
        <?php 
	}
    echo paginate_links( array(
        'base' => $blog_page_url . '%_%',
        'format' => '?page=%#%',
        'current' => max( 1, get_query_var('page') ),
        'total' => $query->max_num_pages
    ) );
} else {
	echo '<p>Sorry, there are no posts to dislay</p>';
}

// Restore original Post Data
wp_reset_postdata();
?>
        </div>

        <?php
        if(is_active_sidebar( 'right_sidebar_blog' )) {
            echo '<div class="chest-right-sidebar-content chest-post-index-right-sidebar col-lg-3">';
                dynamic_sidebar( 'right_sidebar_blog' );
            echo '</div>';
        }
    ?>
        </div>
      </div>

<?php get_footer(); ?>