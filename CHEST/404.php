<?php get_header(); ?>

<?php 
    $requested_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $requested_url_formatted = str_replace('_', ' ', $requested_url);
    $requested_url_formatted = str_replace('-', ' ', $requested_url_formatted);
    $requested_url_formatted = preg_replace('/\\.[^.\\s]{3,4}$/', '', $requested_url_formatted);
    $requested_url_split_array = split('/', $requested_url_formatted);
    $requested_url_keyword = end($requested_url_split_array);
    $suggested_urls = array();

    // WP_Query arguments
    $args = array(
        's' => $requested_url_keyword,
    );

    // The Query
    $query = new WP_Query( $args );

    // The Loop
    $cnt = 0;
    if ( $query->have_posts() ) {
        while ( $query->have_posts() and ($cnt < 3 ) ) {
            $query->the_post();
            array_push($suggested_urls, get_permalink());
            $cnt++;
        }
    }
    wp_reset_postdata();
?>

      <div class="chest-page-title-container container-fluid">
        <h1 class="chest-page-title">404 ERROR</h1>
      </div>
      <div class="chest-main-content-outer container-fluid">
        <div class="chest-main-content-row row">
            <div class="chest-main-content col-lg-9">
                <h2 class="chest-404-subtitle">The page or resource you are looking for may have been removed, had its name changed or is temporarily unavailable.</h2>
                <h2 class="chest-404-sub2">The page you requested:</h2>
                <a href="<?php echo $requested_url; ?>" class="chest-404-link" id="chest-404-requested-link"><?php echo $requested_url; ?></a>
                <?php 
                if( count($suggested_urls) > 0 ) {
                    echo '
                    <h2 class="chest-404-sub2">Did you mean:</h2>        
                    <ul id="chest-404-suggested-list">';
                    foreach($suggested_urls as $suggested_url) {
                      echo '<li><a href="' . $suggested_url . '" class="chest-404-link">' . $suggested_url . '</a></li>';
                  }     
                echo '</ul>';
                }
                ?>
                <p id="chest-404-final-statement">You can also visit the <a href="<?php echo get_home_url(); ?>">Homepage</a> or <a href="mailto:helpteam@chestnet.org">Contact Us</a> about this error.</p>
            </div>
                <?php
                if(is_active_sidebar( 'right_sidebar' )) {
                    echo '<div class="chest-right-sidebar-content col-lg-3">';
                        dynamic_sidebar( 'right_sidebar' );
                    echo '</div>';
                }
                ?>
        </div>
      </div>

<?php get_footer(); ?>