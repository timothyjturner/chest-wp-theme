    <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="8000">
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">

<?php
            wp_reset_postdata();
// WP_Query arguments
$args = array(
	'post_type'              => array( 'chest_sliders' )
);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) {
    $slide_count = 0;
    $slide_count_label = array('First', 'Second', 'Third', 'Fourth', 'Fifth', 'Sixth', 'Seventh', 'Eighth', 'Ninth', 'Tenth');
	while  ($query->have_posts()) {
        $query->the_post();
        $img_attribs = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
        $slider_subtitle = get_post_meta(get_the_ID(), 'subtitle');
        $button_enabled = get_post_meta(get_the_ID(), 'button_enabled');
        $button_text = get_post_meta(get_the_ID(), 'button_text');
        $button_link = get_post_meta(get_the_ID(), 'button_link');
?>
          <div class="carousel-item <?php if($slide_count == 0) { echo 'active'; } ?>">
            <img class="<?php echo $slide_count_label[$slide_count]; ?>-slide" src="<?php echo $img_attribs[0]; ?>" alt="<?php echo $slide_count_label[$slide_count]; ?> slide">
            <div class="container">
              <div class="carousel-caption text-right">
                <h1><?php echo get_the_title(); ?></h1>
                <p><?php echo $slider_subtitle[0]; ?></p>
                <?php if($button_enabled[0]) { ?>
                <p><a class="btn btn-lg btn-primary" href="<?php echo $button_link[0]; ?>" role="button"><?php echo $button_text[0]; ?></a></p>
                <?php
                } ?>                 
              </div>
            </div>
            </div>
<?php
    $slide_count++;
    }
    ?>
    </div>
    <?php
} else {
    ?> 
          <div class="carousel-item active">
            <img class="first-slide" src="<?php echo get_template_directory_uri(); ?>/images/default-banner.jpg" alt="First slide">
            <div class="container">
              <div class="carousel-caption text-right">
                <h1>Example headline.</h1>
                <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <img class="second-slide" src="<?php echo get_template_directory_uri(); ?>/images/default-banner.jpg" alt="Second slide">
            <div class="container">
              <div class="carousel-caption text-right">
                <h1>Another example headline.</h1>
                <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <img class="third-slide" src="<?php echo get_template_directory_uri(); ?>/images/default-banner.jpg" alt="Third slide">
            <div class="container">
              <div class="carousel-caption text-right">
                <h1>One more for good measure.</h1>
                <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
              </div>
            </div>
          </div>
        </div>
    <?php
}

// Restore original Post Data
wp_reset_postdata();

?>

        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
