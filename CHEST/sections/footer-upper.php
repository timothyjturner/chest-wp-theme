         <footer class="container-fluid chest-footer-upper" style="background-color:#<?php echo get_option('chest_large_footer_back_color') ?>;">
            <div class="chest-footer-upper-row row">  
<?php

    $mobileOrder = get_option('chest_large_footer_mobile_order');
    $mobileOrderArray = explode(',', $mobileOrder);

        foreach($mobileOrderArray as $element) {
            switch ($element) {
                case '1':
                    echo '<div class="chest-footer-upper-container chest-footer-upper-1 col-lg-3 hidden-desktop">';
                    if(is_active_sidebar( 'footer_upper_1' )) {
                        dynamic_sidebar( 'footer_upper_1' );
                    } else {
                        echo '<h1 class="chest-upper-footer-heading">ABOUT THE CHEST FOUNDATION</h1><p class="text-style-0">The mission of the CHEST Foundation is to champion lung health by supporting clinical resarch, community service, and patient education.</p>
                    <a href="' . get_home_url() . '/learn-more" class="chest-footer-learn-link"><h3>LEARN MORE</h3></a>';
                    }
                    echo '</div>';
                    break;
                case '2':
                    echo '<div class="chest-footer-upper-container chest-footer-upper-2 col-lg-3 hidden-desktop">';
                    if(is_active_sidebar( 'footer_upper_2' )) {
                        dynamic_sidebar( 'footer_upper_2' );
                    } else {
                        echo '<h1 class="chest-upper-footer-heading">FOLLOW US</h1>
                    <a href="#" target="_blank" class="chest-social-icon-container"><span class="fa-stack fa-lg">
                        <i class="fas fa-circle fa-stack-2x chest-social-circle"></i>
                        <i class="fab fa-facebook-f fa-stack-1x chest-social-icon"></i>
                    </span></a>
                    <a href="#" target="_blank" class="chest-social-icon-container"><span class="fa-stack fa-lg">
                        <i class="fas fa-circle fa-stack-2x chest-social-circle"></i>
                        <i class="fab fa-linkedin-in fa-stack-1x chest-social-icon"></i>
                    </span></a>
                    <a href="#" target="_blank" class="chest-social-icon-container"><span class="fa-stack fa-lg">
                        <i class="fas fa-circle fa-stack-2x chest-social-circle"></i>
                        <i class="fab fa-pinterest-p fa-stack-1x chest-social-icon"></i>
                    </span></a>
                    <a href="#" target="_blank" class="chest-social-icon-container"><span class="fa-stack fa-lg">
                        <i class="fas fa-circle fa-stack-2x chest-social-circle"></i>
                        <i class="fab fa-twitter fa-stack-1x chest-social-icon"></i>
                    </span></a>';
                    }
                    echo '</div>';
                    break;
                case '3':
                    echo '<div class="chest-footer-upper-container chest-footer-upper-3 col-lg-3 hidden-desktop">';
                    if(is_active_sidebar( 'footer_upper_3' )) {
                        dynamic_sidebar( 'footer_upper_3' );
                    } else {
                        echo '<h1 class="chest-upper-footer-heading">CONTACT US</h1>
                    <span class="chest-contact-container"><i class="fas fa-envelope"></i><div class="chest-contact-info"><a href="#" class="text-style-0"> helpteam@chestnet.org</a></div></span>
                <br />
                    <span class="chest-contact-container"><i class="fas fa-phone" data-fa-transform="flip-h"></i><div class="chest-contact-info"><a href="#" class="text-style-0"> 800-343-2227<br /> 224-521-9800</a></div></span>
                <br />
                    <span class="chest-contact-container"><i class="fas fa-comments"></i><div class="chest-contact-info"><a href="#" class="chest-footer-need-help-link"> <h3> NEED HELP NOW?<br /> CLICK TO CHAT</h3></a></div></span>';
                    }
                    echo '</div>';
                    break;
                case '4':
                    echo '<div class="chest-footer-upper-container chest-footer-upper-4 col-lg-3 hidden-desktop">';
                    if(is_active_sidebar( 'footer_upper_4' )) {
                        dynamic_sidebar( 'footer_upper_4' );
                    } else {
                        echo '<h1 class="chest-upper-footer-heading">JOIN OUR EMAIL LIST</h1>
                    <form class="chest-footer-upper-subscribe-form">
                        <input type="text" name="email-sub" placeholder="Email address">
                        <br /><br />
                        <button type="submit">Subscribe</button>
                    </form>';
                    }
                    echo '</div>';
                    break;
            }
        }

        ?>
                <div class="chest-footer-upper-container chest-footer-upper-1 col-lg-3 hidden-mobile">
                    <?php
                    if(is_active_sidebar( 'footer_upper_1' )) {
                        dynamic_sidebar( 'footer_upper_1' );
                    } else {
                        echo '<h1 class="chest-upper-footer-heading">ABOUT THE CHEST FOUNDATION</h1><p class="text-style-0">The mission of the CHEST Foundation is to champion lung health by supporting clinical resarch, community service, and patient education.</p>
                <a href="' . get_home_url() . '/learn-more" class="chest-footer-learn-link"><h3>LEARN MORE</h3></a>';
                    }
                    
                    ?>          
                </div>
                <div class="chest-footer-upper-container chest-footer-upper-2 col-lg-3 hidden-mobile">
                    <?php
                    if(is_active_sidebar( 'footer_upper_2' )) {
                        dynamic_sidebar( 'footer_upper_2' );
                    } else {
                        echo '<h1 class="chest-upper-footer-heading">FOLLOW US</h1>
                    <a href="#" target="_blank" class="chest-social-icon-container"><span class="fa-stack fa-lg">
                        <i class="fas fa-circle fa-stack-2x chest-social-circle"></i>
                        <i class="fab fa-facebook-f fa-stack-1x chest-social-icon"></i>
                    </span></a>
                    <a href="#" target="_blank" class="chest-social-icon-container"><span class="fa-stack fa-lg">
                        <i class="fas fa-circle fa-stack-2x chest-social-circle"></i>
                        <i class="fab fa-linkedin-in fa-stack-1x chest-social-icon"></i>
                    </span></a>
                    <a href="#" target="_blank" class="chest-social-icon-container"><span class="fa-stack fa-lg">
                        <i class="fas fa-circle fa-stack-2x chest-social-circle"></i>
                        <i class="fab fa-pinterest-p fa-stack-1x chest-social-icon"></i>
                    </span></a>
                    <a href="#" target="_blank" class="chest-social-icon-container"><span class="fa-stack fa-lg">
                        <i class="fas fa-circle fa-stack-2x chest-social-circle"></i>
                        <i class="fab fa-twitter fa-stack-1x chest-social-icon"></i>
                    </span></a>';
                    }
                    ?>
                </div>
                <div class="chest-footer-upper-container chest-footer-upper-3 col-lg-3 hidden-mobile">
                    <?php
                    if(is_active_sidebar( 'footer_upper_3' )) {
                        dynamic_sidebar( 'footer_upper_3' );
                    } else {
                        echo '<h1 class="chest-upper-footer-heading">CONTACT US</h1>
                    <span class="chest-contact-container"><i class="fas fa-envelope"></i><div class="chest-contact-info"><a href="#" class="text-style-0"> helpteam@chestnet.org</a></div></span>
                <br />
                    <span class="chest-contact-container"><i class="fas fa-phone" data-fa-transform="flip-h"></i><div class="chest-contact-info"><a href="#" class="text-style-0"> 800-343-2227<br /> 224-521-9800</a></div></span>
                <br />
                    <span class="chest-contact-container"><i class="fas fa-comments"></i><div class="chest-contact-info"><a href="#" class="chest-footer-need-help-link"> <h3> NEED HELP NOW?<br /> CLICK TO CHAT</h3></a></div></span>';
                    }
                    ?>
                </div>
                <div class="chest-footer-upper-container chest-footer-upper-4 col-lg-3 hidden-mobile">
                    <?php
                    if(is_active_sidebar( 'footer_upper_4' )) {
                        dynamic_sidebar( 'footer_upper_4' );
                    } else {
                        echo '<h1 class="chest-upper-footer-heading">JOIN OUR EMAIL LIST</h1>
                <form class="chest-footer-upper-subscribe-form">
                    <input type="text" name="email-sub" placeholder="Email address">
                    <br /><br />
                    <button type="submit">Subscribe</button>
                </form>';
                    }
                    ?>
                </div>
            </div>
      </footer>