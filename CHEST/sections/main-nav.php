      <div class="chest-main-nav-container">   
          <div id="chest-nav-logo-container">
              <a href="<?php echo home_url(); ?>">
                  <img id="chest-nav-logo" src="<?php echo get_option('chest_header_logo'); ?>" />
              </a>
          </div>  
          <nav class="chest-main-nav">
            <?php  
              wp_nav_menu( array(
                    'theme_location' => 'primary_menu',
                    'menu_class'     => 'navbar-nav',
                    'menu_id'     => 'chest-main-nav',
                    'container'     => '',
                    'container_id'     => '',
                    'fallback_cb'    => false 
                ) );
              ?>
          </nav>
      </div> 