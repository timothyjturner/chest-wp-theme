      <footer class="container-fluid chest-footer-lower" style="background-color:#<?php echo get_option('chest_small_footer_back_color') ?>;">
            <div class="chest-footer-lower-row row">
                <div class="chest-footer-copyright col-lg-6 float-lg-left">
                    <?php
                    if(is_active_sidebar( 'footer_lower_1' )) {
                        dynamic_sidebar( 'footer_lower_1' );
                    } else {
                        echo '<p>Copyright 2018 © American College of Chest Physicians ®</p>';
                    }
                    
                    ?> 
                </div>
                <div class="chest-footer-misc-links col-lg-6 float-lg-right">
                    <?php
                    if(is_active_sidebar( 'footer_lower_2' )) {
                        dynamic_sidebar( 'footer_lower_2' );
                    } else {
                    echo '<a href="' . get_home_url() . '/terms">Terms of Use</a>
                    <span class="chest-foolter-lower-separator">|</span>
                    <a href="' . get_home_url() . '/privacy-policy">Privacy Policy</a>';
                    }
                    
                    ?> 
                </div>
                
            </div>
      </footer>