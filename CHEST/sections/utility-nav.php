      <nav class="navbar navbar-expand-lg fixed-top chest-util-nav chest-util-nav-<?php echo get_option('chest_util_menu_style'); ?>">
        <a href="<?php echo home_url(); ?>" class="chest-util-nav-home-link d-none d-lg-block d-xl-block"><img class="chest-util-nav-logo" src="<?php echo get_option('chest_util_menu_icon'); ?>" \></a>
        <button class="navbar-toggler navbar-dark" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
        
            <?php
                wp_nav_menu( array(
                    'theme_location'	=> 'utility_menu',
                    'depth'				=> 2, // 1 = with dropdowns, 0 = no dropdowns.
                    'container'			=> '',
                    'container_class'	=> '',
                    'container_id'		=> '',
                    'menu_class'		=> 'navbar-nav mr-auto',
                    'fallback_cb'		=> 'WP_Bootstrap_Navwalker::fallback',
                    'walker'			=> new WP_Bootstrap_Navwalker()
                ) );
            ?>
                
          <div id="chest-user-nav">
            <div class="chest-user-nav-item" id="chest-nav-search">
                <form class="form-inline mt-2 mt-md-0" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get" role="search">
                    <div id="chest-search-form-container" style="display:none;">
                        <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search" name="s" id="chest-search-input" value="<?php echo get_search_query(); ?>">
                        <button type="submit" id="chest-search-input-button" alt="Search" value="<?php echo esc_attr_x( 'Search', 'submit button' ); ?>">Submit</button>
                    </div>
                </form>
                    <button onclick="showHideSearch()" type="" id="searchsubmit"><i class="fas fa-search"></i></button>
            </div>
            <?php
              if (get_option('chest_cart_enabled') === '1') {
            echo '<a href="#"><div class="chest-user-nav-item" id="chest-nav-cart"><i class="fas fa-shopping-cart"></i>  0</div></a>';
             } ?>
            <a href="#"><div class="chest-user-nav-item" id="chest-nav-user">Log In <i class="fas fa-user"></i><i class="fas fa-caret-down"></i></div></a>
          </div>
            
        </div>
      </nav>